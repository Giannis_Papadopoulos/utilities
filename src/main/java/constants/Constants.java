package constants;


/** Declaration of mathematical constants used throughout the project */
public class Constants {

  /** Epsilon for float comparisons (two float numbers a b are considerer equal if they differ by less than epsilon) */
  public static final float EPSILON_FLOAT = 1E-6f;

  /** Epsilon for double comparisons (two double numbers a b are considerer equal if they differ by less than epsilon) */
  public static final double EPSILON_DOUBLE = 1E-15;
}
