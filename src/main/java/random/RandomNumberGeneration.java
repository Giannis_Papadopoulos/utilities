package random;

import java.util.Random;

/** Utilities for random number generation */
public class RandomNumberGeneration {

  /** Instance of the Random generator */
  private static final Random RANDOM = new Random();

  /** Returns an instance of the random generator with a random seed */
  public static Random getRandomInstance() {
    return RANDOM;
  }

  /**
   * Returns a random int in the range [min, max] inclusive
   */
  public static int diceRoll(int min, int max) {
    int range = max - min + 1;
    int roll = min + RANDOM.nextInt(range);
    return roll;
  }
}
