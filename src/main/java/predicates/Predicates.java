package predicates;


@SuppressWarnings("rawtypes")
public class Predicates {

	/**
	 * The definition of the TRUE and FALSE statement in IPredicate form
	 */
	public static final IPredicate TRUE = new IPredicate() {

		@Override
		public boolean check(Object t) {
			return true;
		}

		@Override
		public String toString() {
			return "true";
		}

	}, FALSE = new IPredicate() {

		@Override
		public boolean check(Object t) {
			return false;
		}

		@Override
		public String toString() {
			return "false";
		}

	};

	/**
	 * Allows construction of a CompositePredicate which is an AND on the given parameter
	 * Predicates
	 * 
	 * @param a
	 *        First of the Predicates to use
	 * @param b
	 *        Second of the Predicates to use
	 * @return The resulting AND Predicate
	 */
	public static <T> AndPredicate<T> and(final IPredicate<T> a, final IPredicate<T> b) {
		return new AndPredicate<T>(a, b);
	}

	/**
	 * Allows construction of a CompositePredicate which is an OR on the given parameter Predicates
	 * 
	 * @param a
	 *        First of the Predicates to use
	 * @param b
	 *        Second of the Predicates to use
	 * @return The resulting OR Predicate
	 */
	public static <T> OrPredicate<T> or(final IPredicate<T> a, final IPredicate<T> b) {
		return new OrPredicate<T>(a, b);
	}

	/**
	 * Allows the construction of a CompositePredicate which is an NOT of the given parameter
	 * Predicate
	 * 
	 * @param a
	 *        The Predicate to NOT
	 * @return The resulting NOT Predicate
	 */
	public static <T> NotPredicate<T> not(final IPredicate<T> a) {
		return new NotPredicate<T>(a);
	}

	// public static <T> ClassPredicate<T> isType(Class<T> type) {
	// return new ClassPredicate<T>(type);
	// }

	public static <T> IPredicate<T> truePredicate(Class<T> clazz) {
		return new IPredicate<T>() {

			@Override
			public boolean check(T t) {
				return true;
			}

			@Override
			public String toString() {
				return "true";
			}
		};
	}

	public static <T> IPredicate<T> truePredicate(T t) {
		return new IPredicate<T>() {

			@Override
			public boolean check(T t) {
				return true;
			}

			@Override
			public String toString() {
				return "true";
			}
		};
	}
	
	public static <T> IPredicate<T> falsePredicate(Class<T> clazz) {
		return new IPredicate<T>() {

			@Override
			public boolean check(T t) {
				return true;
			}

			@Override
			public String toString() {
				return "true";
			}
		};
	}

	public static <T> IPredicate<T> falsePredicate(T t) {
		return new IPredicate<T>() {

			@Override
			public boolean check(T t) {
				return true;
			}

			@Override
			public String toString() {
				return "true";
			}
		};
	}

	/**
	 * Convenience DSL that allows the conversion of a single argument Predicate to work in a paired argument.
	 * 
	 * @param to
	 *        The Predicate to transfer the argument to
	 * @return The resulting Predicate, which accepts a pair, and feeds X to the argument predicate
	 */
	public static <T, E> LeftPredicate<T, E> left(final IPredicate<T> to) {
		return new LeftPredicate<T, E>(to);
	}

	/**
	 * Convenience DSL that allows the conversion of a single argument Predicate to work in a paired argument.
	 * 
	 * @param to
	 *        The Predicate to transfer the argument to
	 * @return The resulting Predicate, which accepts a pair, and feeds Y to the argument predicate
	 */
	public static <T, E> RightPredicate<T, E> right(final IPredicate<E> to) {
		return new RightPredicate<T, E>(to);
	}
}
