package predicates;

import lombok.*;
import lombok.experimental.FieldDefaults;
import datatypes.Pair;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PROTECTED)
public class LeftPredicate<T, E>
		implements IPredicate<Pair<T, E>> {

	IPredicate<T> to;

	public boolean check(Pair<T, E> t) {
		return to.check(t.getLeft());
	}

}
