package predicates;

public interface IPredicate<T> {

	public boolean check(T t);

}
