package predicates;

import lombok.*;
import lombok.experimental.FieldDefaults;
import datatypes.Pair;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PROTECTED)
public class RightPredicate<T, E>
		implements CompositePredicate<Pair<T, E>> {

	IPredicate<E> to;

	public boolean check(Pair<T, E> t) {
		return to.check(t.getRight());
	}

}
