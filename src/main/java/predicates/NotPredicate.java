package predicates;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class NotPredicate<T>
		implements IPredicate<T> {

	IPredicate<T> a;

	public boolean check(T t) {
		return !a.check(t);
	}

}
