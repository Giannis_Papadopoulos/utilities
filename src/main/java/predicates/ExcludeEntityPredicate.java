package predicates;

import lombok.AllArgsConstructor;

import com.artemis.Entity;

/**
 * Predicate used to exclude the given entity
 * 
 * @param excluded
 */
@AllArgsConstructor
public class ExcludeEntityPredicate
		implements IPredicate<Entity> {

	private Entity excluded;

	@Override
	public boolean check(Entity entity) {
		return entity != excluded;
	}

}
