package predicates;

import lombok.AllArgsConstructor;

import com.artemis.Component;
import com.artemis.Entity;

/**
 * Checks if an entity has a specified component
 * 
 * @author Giannis Papadopoulos
 */
@AllArgsConstructor
public class HasComponentPredicate
		implements IPredicate<Entity> {

	/** The class of the component to search for */
	private Class<? extends Component> componentClass;

	@Override
	public boolean check(Entity entity) {
		return entity.getComponent(componentClass) != null;
	}

}
