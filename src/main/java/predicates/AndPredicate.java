package predicates;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class AndPredicate<T>
		implements IPredicate<T> {

	IPredicate<T> a, b;

	public boolean check(T t) {
		return a.check(t) && b.check(t);
	}

}
