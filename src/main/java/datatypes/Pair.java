package datatypes;

import lombok.*;
import lombok.experimental.Accessors;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Accessors(chain = true)
public class Pair<A, B> {

  A x;
  B y;

  /**
   * Nulls all variables in this tuple
   */
  public void reset() {
    this.x = null;
    this.y = null;
  }

  /**
   * Returns an instance of Pair with the given parameters.
   * 
   * @param a
   * @param b
   * @return
   */
  public static <A, B> Pair<A, B> of(A a, B b) {
    return new Pair<A, B>(a, b);
  }

  public A getLeft() {
    return x;
  }

  public B getRight() {
    return y;
  }

}
