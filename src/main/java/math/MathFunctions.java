package math;

import constants.Constants;

/** Library of mathematical functions */
public class MathFunctions {

  /** Returns whether 2 double values are equal */
  public static boolean doubleEquals(double a, double b) {
    return doubleEquals(a, b, Constants.EPSILON_DOUBLE);
  }

  /** Returns whether 2 double values are equal within epsilon */
  public static boolean doubleEquals(double a, double b, double epsilon) {
    return Math.abs(a - b) < epsilon;
  }

  /** Returns whether 2 float values are equal */
  public static boolean floatEquals(float a, float b) {
    return floatEquals(a, b, Constants.EPSILON_FLOAT);
  }

  /** Returns whether 2 float values are equal within epsilon */
  public static boolean floatEquals(float a, float b, float epsilon) {
    return Math.abs(a - b) < epsilon;
  }

  /** Returns whether value is in the interval [lowerBound, upperBound] */
  public static boolean isWithinBounds(double value, double lowerBound, double upperBound) {
    return (value >= lowerBound) && (value <= upperBound);
  }

  /** Returns whether value is in the interval [lowerBound, upperBound] */
  public static boolean isWithinBounds(float value, float lowerBound, float upperBound) {
    return (value >= lowerBound) && (value <= upperBound);
  }

  /** Returns whether value is in the interval [lowerBound, upperBound] */
  public static boolean isWithinBounds(int value, int lowerBound, int upperBound) {
    return (value >= lowerBound) && (value <= upperBound);
  }

  /** Constrain val in interval [min, max] */
  public static int constrain(int min, int max, int val) {
    return Math.max(Math.min(val, max), min);
  }

  /** Constrain val in interval [min, max] */
  public static double constrain(double min, double max, double val) {
    return Math.max(Math.min(val, max), min);
  }

  /** Constrain val in interval [min, max] */
  public static float constrain(float min, float max, float val) {
    return Math.max(Math.min(val, max), min);
  }

  /** Returns the median of the 3 values */
  public static int median(int a, int b, int c) {
    if ((a <= b && a >= c) || (a >= b && a <= c))
      return a;
    else if ((b <= a && b >= c) || (b >= a && b <= c))
      return b;
    else
      return c;
  }

  /**
   * Return sum of all values in array.
   */
  public static float sum(float[] a) {
    float sum = 0.0f;
    for (int i = 0; i < a.length; i++) {
      sum += a[i];
    }
    return sum;
  }

  /**
   * Return average value in array, NaN if no such value.
   */
  public static float mean(float[] a) {
    if (a.length == 0)
      return Float.NaN;
    float sum = sum(a);
    return sum / a.length;
  }

}
