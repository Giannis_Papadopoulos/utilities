package math;

import static constants.Constants.EPSILON_DOUBLE;
import static constants.Constants.EPSILON_FLOAT;
import static org.junit.Assert.*;

import org.junit.Test;

public class MathFunctionsTest {

  @Test
  public void testConstrain() {
    assertEquals(45, MathFunctions.constrain(0, 100, 45));
    assertEquals(0, MathFunctions.constrain(0, 100, -1));
    assertEquals(0, MathFunctions.constrain(0, 100, 0));
    assertEquals(100, MathFunctions.constrain(0, 100, 150));
    assertEquals(100, MathFunctions.constrain(0, 100, 100));

    assertEquals(0.45, MathFunctions.constrain(0, 1.0, 0.45), EPSILON_DOUBLE);
    assertEquals(0.0, MathFunctions.constrain(0, 1.0, -0.45), EPSILON_DOUBLE);
    assertEquals(0.0, MathFunctions.constrain(0, 1.0, 0.0), EPSILON_DOUBLE);
    assertEquals(1.0, MathFunctions.constrain(0, 1.0, 1.0), EPSILON_DOUBLE);
    assertEquals(1.0, MathFunctions.constrain(0, 1.0, 1.2), EPSILON_DOUBLE);

    assertEquals(0.45f, MathFunctions.constrain(0, 1.0f, 0.45f), EPSILON_FLOAT);
    assertEquals(0.0f, MathFunctions.constrain(0, 1.0f, -0.45f), EPSILON_FLOAT);
    assertEquals(0.0f, MathFunctions.constrain(0, 1.0f, 0.0f), EPSILON_FLOAT);
    assertEquals(1.0f, MathFunctions.constrain(0, 1.0f, 1.0f), EPSILON_FLOAT);
    assertEquals(1.0f, MathFunctions.constrain(0, 1.0f, 1.2f), EPSILON_FLOAT);

  }

  @Test
  public void testDoubleEquals() {
    assertTrue(MathFunctions.doubleEquals(1.0, 1.0));
    assertTrue(MathFunctions.doubleEquals(1.0, 1.0000000000000001));
    assertTrue(MathFunctions.doubleEquals(1.0, 0.9999999999999999));
    assertFalse(MathFunctions.doubleEquals(1.0, 1.000000000000001));
    assertFalse(MathFunctions.doubleEquals(1.0, 0.999999999999998));
  }

  @Test
  public void testDoubleEqualsWithinEpsilon() {
    double epsilon = 0.1;
    assertTrue(MathFunctions.doubleEquals(1.0, 1.0, epsilon));
    assertTrue(MathFunctions.doubleEquals(1.0, 1.1 - EPSILON_DOUBLE, epsilon));
    assertTrue(MathFunctions.doubleEquals(1.0, 0.9 + EPSILON_DOUBLE, epsilon));
    assertFalse(MathFunctions.doubleEquals(1.0, 1.11, epsilon));
    assertFalse(MathFunctions.doubleEquals(1.0, 0.89, epsilon));
  }

  @Test
  public void testFloatEquals() {
    assertTrue(MathFunctions.floatEquals(1.0f, 1.0f));
    assertTrue(MathFunctions.floatEquals(1.0f, 1.000001f));
    assertTrue(MathFunctions.floatEquals(1.0f, 0.9999991f));
    assertFalse(MathFunctions.doubleEquals(1.0, 1.000001));
    assertFalse(MathFunctions.doubleEquals(1.0, 0.999999));
  }

  @Test
  public void testFloatEqualsWithinEpsilon() {
    float epsilon = 0.1f;
    assertTrue(MathFunctions.floatEquals(1.0f, 1.0f, epsilon));
    assertTrue(MathFunctions.floatEquals(1.0f, 1.1f - EPSILON_FLOAT, epsilon));
    assertTrue(MathFunctions.floatEquals(1.0f, 0.9f + EPSILON_FLOAT, epsilon));
    assertFalse(MathFunctions.doubleEquals(1.0f, 1.11f, epsilon));
    assertFalse(MathFunctions.doubleEquals(1.0f, 0.89f, epsilon));
  }

  @Test
  public void testMedian() {
    assertEquals(2, MathFunctions.median(1, 2, 3));
    assertEquals(2, MathFunctions.median(1, 3, 2));
    assertEquals(2, MathFunctions.median(2, 1, 3));
    assertEquals(2, MathFunctions.median(2, 3, 1));
    assertEquals(2, MathFunctions.median(3, 1, 2));
    assertEquals(2, MathFunctions.median(3, 2, 1));

    assertEquals(1, MathFunctions.median(1, 1, 2));
    assertEquals(1, MathFunctions.median(1, 2, 1));
    assertEquals(1, MathFunctions.median(2, 1, 1));
    assertEquals(2, MathFunctions.median(2, 2, 2));

  }
}
